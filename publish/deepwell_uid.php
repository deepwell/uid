<?php

declare(strict_types=1);

return [
    // 当前时间,相对于时间基点的增量,单位秒
    'timestampBits' => 28,
    // 机器ID
    'workerIdBits' => 22,
    // 每秒下的并发序列
    'sequenceBits' => 13,
    // 时间基点 2022-11-26
    'epochSeconds' => 1669392000,
    // Buffer size扩容参数, 可提高UID生成的吞吐量
    // 原bufferSize=8192, 扩容后bufferSize= 8192 << 3 = 65536
    'boostPower' => 3,
    // 指定何时向Buffer中填充UID, 取值为百分比(0, 100), 默认为50
    // 举例: bufferSize=1024, paddingPercent=50 -> threshold=1024 * 50 / 100 = 512
    // 当通道中UID数量 < 512时, 将自动对Buffer进行填充补全
    'paddingPercent' => 50,
    // 另外一种Buffer填充时机, 周期性检查填充 -->
    // 默认:不配置此项, 即不使用用Schedule. 如需使用, 请指定Schedule时间间隔, 单位:秒 -->
    //'scheduleInterval'=>60,
];
