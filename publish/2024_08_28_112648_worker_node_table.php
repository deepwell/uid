<?php

use Hyperf\Database\Migrations\Migration;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Schema\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('dw_uid_worker_node', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->nullable(false);
            $table->string('hostname', 64)->nullable(false);
            $table->string('port', 64)->nullable(false);
            $table->enum('type', ['actual', 'container']);
            $table->dateTime('created_at')->nullable(false);
            $table->dateTime('updated_at')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::drop('dw_uid_worker_node');
    }
};
