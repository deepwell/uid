<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid;

use Hyperf\Config\Annotation\Value;
use RuntimeException;

class BitsAllocator
{
    /**
     * Total 64 bits
     */
    final const TOTAL_BITS = 1 << 6;

    /** Customer epoch, unit as second. For example 2022-11-26 (s: 1669392000)*/
    #[Value('deepwell_uid.epochSeconds')]
    private int $epochSeconds = 1669392000;

    /**
     * Bits for [sign-> second-> workId-> sequence]
     */
    private int $signBits = 1;

    #[Value('deepwell_uid.timestampBits')]
    private int $timestampBits = 28;

    #[Value('deepwell_uid.workerIdBits')]
    private int $workerIdBits = 22;

    #[Value('deepwell_uid.sequenceBits')]
    private int $sequenceBits = 13;

    private int $workerId;

    /**
     * Max value for workId & sequence
     */
    private readonly int $maxDeltaSeconds;
    private readonly int $maxWorkerId;
    private readonly int $maxSequence;

    /**
     * Shift for timestamp & workerId
     */
    private readonly int $timestampShift;
    private readonly int $workerIdShift;

    public function __construct()
    {
        // make sure allocated 64 bits
        $allocateTotalBits = $this->signBits + $this->timestampBits + $this->workerIdBits + $this->sequenceBits;
        assert($allocateTotalBits == self::TOTAL_BITS, "Allocate not enough 64 bits");

        // initialize max value
        $this->maxDeltaSeconds = ~(-1 << $this->timestampBits);
        $this->maxWorkerId = ~(-1 << $this->workerIdBits);
        $this->maxSequence = ~(-1 << $this->sequenceBits);

        // initialize shift
        $this->timestampShift = $this->workerIdBits + $this->sequenceBits;
        $this->workerIdShift = $this->sequenceBits;
    }

    /**
     *  Allocate bits for UID according to delta seconds & workerId & sequence<br>
     * <b>Note that: </b>The highest bit will always be 0 for sign
     * @param int $currentTime
     * @param int $sequence
     * @return int
     */
    public function allocate(int $currentTime, int $sequence): int
    {
        $deltaSeconds = $currentTime - $this->epochSeconds;
        return ($deltaSeconds << $this->timestampShift) | ($this->workerId << $this->workerIdShift) | $sequence;
    }

    /**
     * Getters
     */
    public function getSignBits(): int
    {
        return $this->signBits;
    }

    public function getTimestampBits(): int
    {
        return $this->timestampBits;
    }

    public function getWorkerIdBits(): int
    {
        return $this->workerIdBits;
    }

    public function getSequenceBits(): int
    {
        return $this->sequenceBits;
    }

    public function getEpochSeconds(): int
    {
        return $this->epochSeconds;
    }

    public function getWorkerId(): int
    {
        return $this->workerId;
    }

    public function setWorkerId(int $workerId): void
    {
        if ($workerId > $this->maxWorkerId) {
            throw new RuntimeException('Worker id: ' . $workerId . ' exceeds the max: ' . $this->maxWorkerId);
        }
        $this->workerId = $workerId;
    }

    public function getMaxWorkerId(): int
    {
        return $this->maxWorkerId;
    }

    public function getMaxDeltaSeconds(): int
    {
        return $this->maxDeltaSeconds;
    }

    public function getMaxSequence(): int
    {
        return $this->maxSequence;
    }

    public function getTimestampShift(): int
    {
        return $this->timestampShift;
    }

    public function getWorkerIdShift(): int
    {
        return $this->workerIdShift;
    }

}