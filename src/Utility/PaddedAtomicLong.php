<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Utility;

use Swoole\Atomic\Long;

/**
 * Represents a padded \Swoole\Atomic\Long to prevent the FalseSharing problem<p>
 * The CPU cache line commonly be 64 bytes, here is a sample of cache line after padding:<br>
 * 64 bytes = 8 bytes (object reference) + 6 * 8 bytes (padded long) + 8 bytes (a long value)
 */
class PaddedAtomicLong extends Long
{
    /** Padded 6 long (48 bytes) */
    public int $p1 = 1, $p2 = 2, $p3 = 3, $p4 = 4, $p5 = 5, $p6 = 6;

    public function __construct(int $initValue = 0)
    {
        parent::__construct($initValue);
    }

    /**
     * To prevent GC optimizations for cleaning unused padded references
     */
    public function sumPaddingToPreventOptimization(): int
    {
        return $this->p1 + $this->p2 + $this->p3 + $this->p4 + $this->p5 + $this->p6;
    }
}