<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Utility;

class DockerUtility
{
    final const string ENV_KEY_HOSTNAME = 'HOSTNAME';

    /** Docker host & port */
    private static string $hostname;
    private static string $port;

    /** Whether is docker */
    private static bool $isDocker;


    /**
     * Retrieve docker host
     *
     * @return string empty string if not a docker
     */
    public static function getHostname(): string
    {
        if (isset(self::$hostname)) {
            return self::$hostname;
        }

        self::$hostname = self::isDocker() ? getenv(self::ENV_KEY_HOSTNAME) : '';

        return self::$hostname;
    }

    /**
     * Whether a docker
     *
     * @return bool
     */
    public static function isDocker(): bool
    {
        if (isset(self::$isDocker)) {
            return self::$isDocker;
        }

        self::$isDocker = file_exists('/.dockerenv');

        return self::$isDocker;
    }

    /**
     * Retrieve docker port
     *
     * @return string empty string if not a docker
     */
    public static function getPort(): string
    {
        if (isset(self::$port)) {
            return self::$port;
        }

        // first server config
        $serverConf = config('server') ?? [];
        self::$port = (string)$serverConf['servers'][0]['port'];
        return self::$port;
    }
}