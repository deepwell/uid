<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Listener;

use Deepwell\HyperfUid\Contract\GeneratorInterface;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BeforeMainServerStart;
use Hyperf\Server\Event\MainCoroutineServerStart;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

#[Listener]
class InitListener implements ListenerInterface
{

    #[Inject]
    protected ContainerInterface $container;

    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            BeforeMainServerStart::class,
            MainCoroutineServerStart::class,
        ];
    }

    /**
     * @param $event
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function process($event): void
    {
        // PaddingExecutor RingBuffer 使用了Swoole Atomic, 需要在server start前初始化
        $generator = $this->container->get(GeneratorInterface::class);

        // 如果是协程风格服务, 需要在主协程服务启动时做初始化工作,
        // 防止涉及到存储过程提示报错(比如Mysql协程客户端只能在协程环境中使用)
        $generator->init();
    }
}