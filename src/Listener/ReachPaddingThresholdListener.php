<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Listener;

use Deepwell\HyperfUid\Buffer\PaddingExecutor;
use Deepwell\HyperfUid\Event\ReachPaddingThreshold;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Event\Annotation\Listener;
use Hyperf\Event\Contract\ListenerInterface;

#[Listener]
class ReachPaddingThresholdListener implements ListenerInterface
{

    #[Inject]
    protected ContainerInterface $container;

    public function listen(): array
    {
        return [
            ReachPaddingThreshold::class,
        ];
    }

    public function process($event): void
    {
        $this->container->get(PaddingExecutor::class)->asyncPadding();
    }
}