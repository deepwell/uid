<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid;

use Deepwell\HyperfUid\Contract\GeneratorInterface;
use Hyperf\Context\ApplicationContext;

/**
 * @mixin \Hyperf\Database\Model\Model
 */
trait Snowflake
{
    public function creating(): void
    {
        if (!$this->getKey()) {
            $container = ApplicationContext::getContainer();
            $generator = $container->get(GeneratorInterface::class);
            $this->{$this->getKeyName()} = $generator->getUid();
        }
    }

    public function getIncrementing(): false
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'int';
    }
}