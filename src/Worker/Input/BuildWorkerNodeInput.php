<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Worker\Input;

use Deepwell\Data\Attributes\WithCast;
use Deepwell\Data\Casts\EnumCast;
use Deepwell\Data\Data;
use Deepwell\HyperfUid\Worker\Enum\NodeTypeEnum;

class BuildWorkerNodeInput extends Data
{
    public string $hostname;

    #[WithCast(EnumCast::class)]
    public NodeTypeEnum $type;

    public string $port;
}