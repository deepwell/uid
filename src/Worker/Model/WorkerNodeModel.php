<?php

declare(strict_types=1);

namespace Deepwell\HyperfUid\Worker\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property string $hostname
 * @property string $port
 * @property string $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class WorkerNodeModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var ?string
     */
    protected ?string $table = 'dw_uid_worker_node';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected array $fillable = ['id', 'hostname', 'port', 'type', 'created_at', 'updated_at'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected array $casts = ['id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}
