<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Worker;

use Deepwell\HyperfUid\Contract\WorkerIdAssignerInterface;
use Deepwell\HyperfUid\Contract\WorkerIdAssignerServiceInterface;
use Deepwell\HyperfUid\Utility\DockerUtility;
use Deepwell\HyperfUid\Worker\Enum\NodeTypeEnum;
use Deepwell\HyperfUid\Worker\Input\BuildWorkerNodeInput;
use Deepwell\HyperfUid\Worker\Model\WorkerNodeModel;
use Deepwell\HyperfUid\Worker\Output\BuildWorkerNodeOutput;
use Swoole\Exception;

class DisposableWorkerIdAssigner implements WorkerIdAssignerInterface
{

    /**
     * Assign worker id base on database.<p>
     * If there is hostname in the environment, we considered that the node runs in Docker container<br>
     * Otherwise, the node runs on an actual machine.
     */
    public function assignWorkerId(): int
    {
        if (true === (bool)config('deepwell_uid.isWorkerIdCenter')) {
            // build worker node entity
            $workerNode = $this->buildWorkerNode();
            return $workerNode->id;
        } else {
            return (int)container()->get(WorkerIdAssignerServiceInterface::class)->assignWorkerId($this->newWorkerNode()->toArray());
        }
    }

    /**
     * Build worker node entity by IP and PORT
     */
    public function buildWorkerNode(?BuildWorkerNodeInput $input = null): BuildWorkerNodeOutput
    {
        if ($input) {
            $workerNode = new WorkerNodeModel();
            $workerNode->fill($input->toArray());
        } else {
            $workerNode = $this->newWorkerNode();
        }
        if (!$workerNode->save()) {
            throw new Exception('Add worker node failed');
        }

        return BuildWorkerNodeOutput::from($workerNode);
    }

    protected function newWorkerNode(): WorkerNodeModel
    {
        $workerNode = new WorkerNodeModel();

        if (DockerUtility::isDocker()) {
            $workerNode->type = NodeTypeEnum::Container->value;
            $workerNode->hostname = DockerUtility::getHostname();
        } else {
            $workerNode->type = NodeTypeEnum::Actual->value;
            $workerNode->hostname = gethostbyname(gethostname());
        }

        $workerNode->port = DockerUtility::getPort() . microtime(true) . mt_rand(1, 10000);

        return $workerNode;
    }
}