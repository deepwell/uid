<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Worker\Enum;

enum NodeTypeEnum: string
{
    case Actual = 'actual';

    case Container = 'container';
}