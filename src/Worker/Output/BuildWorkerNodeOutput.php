<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Worker\Output;

use Deepwell\Data\Data;

class BuildWorkerNodeOutput extends Data
{
    public int $id;
}