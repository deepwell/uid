<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Generator;

use Deepwell\HyperfUid\Buffer\PaddingExecutor;
use Deepwell\HyperfUid\Contract\BufferInterface;
use Hyperf\Config\Annotation\Value;

class CachedGenerator extends DefaultGenerator
{
    /** Buffer size扩容参数, 可提高UID生成的吞吐量 */
    #[Value('deepwell_uid.boostPower')]
    protected int $boostPower = 3;

    /**
     * Threshold for trigger padding buffer<br>
     * 指定何时向ChannelBuffer中填充UID, 取值为百分比(0, 100), 默认为50<br>
     * 举例: bufferSize=1024, paddingPercent=50 -> threshold=1024 * 50 / 100 = 512<br>
     * 当通道中UID数量少于512时, 将自动对Channel进行填充补全
     */
    #[Value('deepwell_uid.paddingPercent')]
    protected int $paddingPercent = 50;

    /**
     * 另外一种Buffer填充时机, 周期性检查填充<br>
     * 默认:不配置此项, 即不使用用Schedule. 如需使用, 请指定Schedule时间间隔, 单位:秒
     */
    #[Value('deepwell_uid.scheduleInterval')]
    private ?int $scheduleInterval;

    private readonly BufferInterface $buffer;

    private readonly PaddingExecutor $paddingExecutor;


    public function __construct()
    {
        parent::__construct();

        assert($this->boostPower > 0, "Boost power must be positive");
        assert($this->paddingPercent > 0 && $this->paddingPercent < 100, "Padding percent must between 0 and 100");
        $this->preparedBuffer();
    }

    /**
     * Initialize Buffer & PaddingExecutor
     */
    protected function preparedBuffer()
    {
        // initialize RingBuffer
        $bufferSize = ($this->bitsAllocator->getMaxSequence() + 1) << $this->boostPower;
        //assert(($bufferSize & $bufferSize - 1) == 0, "Buffer size must be a power of 2");
        $paddingThreshold = $bufferSize * $this->paddingPercent / 100;
        $this->buffer = $this->container->make(BufferInterface::class, [$bufferSize, $paddingThreshold]);
        $this->container->set(BufferInterface::class, $this->buffer);
        $this->logger->info(sprintf('Initialized buffer size: %d, paddingPercent: %d', $bufferSize, $paddingThreshold));

        // initialize PaddingExecutor
        $this->paddingExecutor = $this->container->get(PaddingExecutor::class);
        //$paddingExecutor->setBuffer($this->buffer);
        $this->paddingExecutor->setScheduleInterval(empty($this->scheduleInterval) ? 0 : $this->scheduleInterval);
    }

    public function init(): void
    {
        parent::init();

        $this->paddingExecutor->paddingBuffer();
        $this->paddingExecutor->start();
    }

    public function getUid(): int
    {
        return $this->buffer->take();
    }
}