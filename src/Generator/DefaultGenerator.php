<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Generator;

use Deepwell\HyperfUid\BitsAllocator;
use Deepwell\HyperfUid\Contract\GeneratorInterface;
use Deepwell\HyperfUid\Contract\WorkerIdAssignerInterface;
use Exception;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\Inject;

class DefaultGenerator implements GeneratorInterface
{
    #[Inject]
    protected ContainerInterface $container;

    #[Inject]
    protected StdoutLoggerInterface $logger;

    #[Inject]
    protected BitsAllocator $bitsAllocator;

    /** Volatile fields caused by nextId() */
    protected int $sequence = 0;

    protected int $lastSecond = -1;

    public function __construct()
    {

    }


    public function init(): void
    {
        // 1.注册worker id
        $workerIdAssigner = $this->container->get(WorkerIdAssignerInterface::class);
        $workerId = $workerIdAssigner->assignWorkerId();

        // 2.分配worker id, 之后才能结合当前时间戳和序列号生成UID
        $this->bitsAllocator->setWorkerId($workerId);
        $this->logger->info('Registered UidGenerator worker id: ' . $workerId);
    }


    public function getUid(): int
    {
        return $this->nextId();
    }

    /**
     * Get UID
     */
    protected function nextId(): int
    {
        $currentSecond = $this->getCurrentSecond();

        // Clock moved backwards, refuse to generate uid
        if ($currentSecond < $this->lastSecond) {
            $refusedSeconds = $this->lastSecond - $currentSecond;
            throw new Exception('Clock moved backwards. Refusing for ' . $refusedSeconds . ' seconds');
        }

        // At the same second, increase sequence
        if ($currentSecond == $this->lastSecond) {

            $this->sequence = ($this->sequence + 1) & $this->bitsAllocator->getMaxSequence();
            // Exceed the max sequence, we wait the next second to generate uid
            if ($this->sequence == 0) {
                $currentSecond = $this->getNextSecond($this->lastSecond);
            }

            // At the different second, sequence restart from zero
        } else {
            $this->sequence = 0;
        }

        $this->lastSecond = $currentSecond;

        // Allocate bits for UID
        return $this->bitsAllocator->allocate($currentSecond, $this->sequence);
    }

    /**
     * Get current second
     */
    private function getCurrentSecond(): int
    {
        $currentSecond = time();
        if ($currentSecond - $this->bitsAllocator->getEpochSeconds() > $this->bitsAllocator->getMaxDeltaSeconds()) {
            throw new Exception('Timestamp bits is exhausted. Refusing UID generate. Now: ' . $currentSecond);
        }

        return $currentSecond;
    }

    /**
     * Get next second
     */
    private function getNextSecond(int $lastTimestamp): int
    {
        $timestamp = $this->getCurrentSecond();
        while ($timestamp <= $lastTimestamp) {
            $timestamp = $this->getCurrentSecond();
        }

        return $timestamp;
    }

    public function parseUid(int $uid): string
    {
        $workerIdBits = $this->bitsAllocator->getWorkerIdBits();
        $sequenceBits = $this->bitsAllocator->getSequenceBits();

        $shiftDeltaSeconds = $uid >> $this->bitsAllocator->getTimestampShift();
        $shiftWorkerId = $uid >> $this->bitsAllocator->getWorkerIdShift();

        // parse UID
        $deltaSeconds = $uid >> ($workerIdBits + $sequenceBits);
        $workerId = $shiftDeltaSeconds << $workerIdBits ^ $shiftWorkerId;
        $sequence = $shiftWorkerId << $sequenceBits ^ $uid;

        $thatTime = $this->bitsAllocator->getEpochSeconds() + $deltaSeconds;
        $thatTimeStr = date('Y-m-d H:i:s', $thatTime);

        // format as string
        return sprintf("{\"UID\":\"%d\",\"timestamp\":\"%s\",\"workerId\":\"%d\",\"sequence\":\"%d\"}",
            $uid, $thatTimeStr, $workerId, $sequence);
    }
}