<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Event;

use Deepwell\HyperfUid\Contract\BufferInterface;

/**
 * Reach the padding threshold event
 */
readonly class ReachPaddingThreshold
{
    /**
     * @param BufferInterface $buffer
     */
    public function __construct(private BufferInterface $buffer)
    {
    }

    public function getBuffer(): BufferInterface
    {
        return $this->buffer;
    }
}