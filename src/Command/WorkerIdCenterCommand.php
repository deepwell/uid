<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Command;

use Hyperf\Command\Command;
use Symfony\Component\Console\Input\InputArgument;

#[\Hyperf\Command\Annotation\Command]
class WorkerIdCenterCommand extends Command
{
    protected ?string $name = 'deepwell-uid:worker-id-center';

    protected string $configPath = BASE_PATH . '/config/autoload/deepwell_uid.php';

    public function configure(): void
    {
        parent::configure();
        $this->setDescription('worker id 分发中心设置');
        $this->setHelp('worker id 分发中心设置');
        $this->addArgument('action', InputArgument::REQUIRED, 'up: 设置为分发中心, down: 取消分发中心');
    }

    public function handle(): void
    {
        match ($this->argument('action')) {
            'up' => $this->up(),
            'down' => $this->down(),
        };
    }

    public function up(): void
    {
        // 定义源文件和目标文件路径
        $sourceFile = __DIR__ . '/stubs/WorkerIdAssignerService.php.sub';
        $targetFile = __DIR__ . '/../Rpc/WorkerIdAssignerService.php';
        // 创建目录
        $dir = dirname($targetFile);
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        // 复制并重命名文件
        if (file_exists($targetFile)) {
            unlink($targetFile);
        }
        if (copy($sourceFile, $targetFile)) {
            $this->line('WorkerIdAssignerService RPC 文件复制成功！', 'info');
        } else {
            $this->line('WorkerIdAssignerService RPC 文件复制失败！', 'info');
        }

        // 更新配置
        $array = $this->getConfigArray();
        // 检查 'isWorkerIdCenter' 字段
        if (!isset($array['isWorkerIdCenter']) || boolval($array['isWorkerIdCenter']) !== true) {
            // 修改数组
            $array['isWorkerIdCenter'] = 1;
            $this->editConfig($array);
        }
    }

    protected function getConfigArray()
    {
        // 文件路径
        $filePath = BASE_PATH . '/config/autoload/deepwell_uid.php';
        if (!file_exists($filePath)) {
            $array = [];
        } else {
            // 将内容转换为数组
            $array = require($filePath);
        }

        return $array;
    }

    protected function editConfig(array $newCfgArr): void
    {
        // 将数组转换回内容
        $replacedContent = [];
        foreach ($newCfgArr as $key => $value) {
            $replacedContent[] = '{{' . $key . '}}';
        }
        $newContent = str_replace($replacedContent, array_values($newCfgArr), file_get_contents(__DIR__ . '/stubs/deepwell_uid.php.stub'));
        // 写回文件
        file_put_contents($this->configPath, $newContent);
    }

    public function down(): void
    {
        $targetFile = __DIR__ . '/../JsonRpc/WorkerIdAssignerService.php';
        if (file_exists($targetFile)) {
            unlink($targetFile);
        }
        $this->line('WorkerIdAssignerService RPC 文件删除成功！', 'info');

        // 更新配置
        $array = $this->getConfigArray();
        // 检查 'isWorkerIdCenter' 字段
        if (isset($array['isWorkerIdCenter']) && boolval($array['isWorkerIdCenter']) !== false) {
            // 修改数组
            $array['isWorkerIdCenter'] = 0;
            $this->editConfig($array);
        }
    }
}