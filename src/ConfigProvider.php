<?php

declare(strict_types=1);

namespace Deepwell\HyperfUid;

use Deepwell\HyperfUid\Buffer\BufferedUidProvider;
use Deepwell\HyperfUid\Buffer\RingBuffer;
use Deepwell\HyperfUid\Contract\BufferedUidProviderInterface;
use Deepwell\HyperfUid\Contract\BufferInterface;
use Deepwell\HyperfUid\Contract\GeneratorInterface;
use Deepwell\HyperfUid\Contract\WorkerIdAssignerInterface;
use Deepwell\HyperfUid\Generator\CachedGenerator;
use Deepwell\HyperfUid\Worker\DisposableWorkerIdAssigner;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            'dependencies' => [
                WorkerIdAssignerInterface::class => DisposableWorkerIdAssigner::class,
                BufferedUidProviderInterface::class => BufferedUidProvider::class,
                BufferInterface::class => RingBuffer::class,
                GeneratorInterface::class => CachedGenerator::class,
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'The config of deepwell uid.',
                    'source' => __DIR__ . '/../publish/deepwell_uid.php',
                    'destination' => BASE_PATH . '/config/autoload/deepwell_uid.php',
                ],
                [
                    'id' => 'dw_uid_worker_node_migration',
                    'description' => 'worker node migration',
                    'source' => __DIR__ . '/../publish/2024_08_28_112648_worker_node_table.php',
                    'destination' => BASE_PATH . '/migrations/2024_08_28_112648_worker_node_table.php'
                ],
            ],
        ];
    }
}
