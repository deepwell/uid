<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Buffer;

use Deepwell\HyperfUid\Contract\BufferedUidProviderInterface;
use Deepwell\HyperfUid\Contract\BufferInterface;
use Deepwell\HyperfUid\Utility\PaddedAtomicLong;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\Inject;
use Swoole\Atomic;
use Swoole\Timer;
use function Hyperf\Coroutine\co;

class PaddingExecutor
{
    #[Inject]
    protected StdoutLoggerInterface $logger;

    /** Whether buffer padding is running */
    private readonly Atomic $running;

    /** We can borrow UIDs from the future, here store the last second we have consumed */
    private readonly PaddedAtomicLong $lastSecond;

    /** UID Buffer */
    #[Inject]
    private readonly BufferedUidProviderInterface $uidProvider;

    /** UID UID provider */
    #[Inject]
    private readonly BufferInterface $buffer;

    /** Schedule interval Unit as seconds */
    private int $scheduleInterval;
    private int $scheduleId;


    public function __construct()
    {
        $this->running = new Atomic(0);
        $this->lastSecond = new PaddedAtomicLong(time());
        $this->logger->info('PaddingExecutor prepared,lastSecond: ' . $this->lastSecond->get());
    }

    /**
     * Start executors such as schedule
     */
    public function start(): void
    {
        if (!empty($this->scheduleInterval) && empty($this->scheduleId)) {
            $this->scheduleId = Timer::tick($this->scheduleInterval * 1000, [$this, 'paddingBuffer']);
            $this->logger->info('PaddingBuffer schedule start, will trigger every ' . $this->scheduleInterval . ' seconds');
        }
    }

    public function asyncPadding(): void
    {
        co(function () {
            $this->paddingBuffer();
        });
    }

    /**
     * Padding buffer fill the slots until to catch the cursor
     */
    public function paddingBuffer(): void
    {
        if (!isset($this->buffer)) {
            $this->logger->warning('Buffer not set for PaddingExecutor');
            return;
        }

        if (!isset($this->uidProvider)) {
            $this->logger->warning('UidProvider not set for PaddingExecutor');
            return;
        }

        // is still running
        if (!$this->running->cmpset(0, 1)) {
            //$this->logger->info('Padding buffer is still running. ' . $this->buffer);
            return;
        }

        $this->logger->info(sprintf('Ready to padding buffer lastSecond: %s. %s', date('Y-m-d H:i:s', $this->lastSecond->get()), $this->buffer));

        // fill the rest slots until to catch the cursor
        $isFullRingBuffer = false;
        while (!$isFullRingBuffer) {
            $uidList = $this->uidProvider->provide($this->lastSecond->add());
            foreach ($uidList as $uid) {
                $isFullRingBuffer = !$this->buffer->put($uid);
                if ($isFullRingBuffer) {
                    break;
                }
            }
        }

        $this->logger->info(sprintf('End to padding buffer lastSecond: %s. %s', date('Y-m-d H:i:s', $this->lastSecond->get()), $this->buffer));
        // not running now
        $this->running->cmpset(1, 0);
    }

    public function setScheduleInterval(int $scheduleInterval): void
    {
        assert($scheduleInterval > 0, "Schedule interval must positive!");
        $this->scheduleInterval = $scheduleInterval;
    }
}