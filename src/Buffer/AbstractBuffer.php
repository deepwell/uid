<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Buffer;

use Deepwell\HyperfUid\Contract\BufferInterface;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\Inject;
use Psr\EventDispatcher\EventDispatcherInterface;
use Stringable;

abstract class AbstractBuffer implements BufferInterface, Stringable
{
    #[Inject]
    protected ContainerInterface $container;

    #[Inject]
    protected StdoutLoggerInterface $logger;

    #[Inject]
    protected EventDispatcherInterface $eventDispatcher;

    /**
     * @param int $bufferSize The size of Buffer's slots, each slot hold a UID
     * @param int $paddingThreshold Threshold for trigger padding buffer
     */
    public function __construct(protected int $bufferSize, protected int $paddingThreshold)
    {
        assert(($this->bufferSize & $this->bufferSize - 1) == 0, "Buffer size must be a power of 2");
        assert($this->paddingThreshold > 0, "Padding threshold must be positive");
    }
}