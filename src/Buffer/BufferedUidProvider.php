<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Buffer;

use Deepwell\HyperfUid\BitsAllocator;
use Deepwell\HyperfUid\Contract\BufferedUidProviderInterface;
use Hyperf\Di\Annotation\Inject;

class BufferedUidProvider implements BufferedUidProviderInterface
{
    #[Inject]
    private readonly BitsAllocator $bitsAllocator;

    public function provide(int $momentInSecond): array
    {
        return $this->nextIdsForOneSecond($momentInSecond);
    }

    /**
     * Get the UIDs in the same specified second under the max sequence
     * @param int $currentTime
     * @return array UID list
     */
    protected function nextIdsForOneSecond(int $currentTime): array
    {
        $listSize = $this->bitsAllocator->getMaxSequence() + 1;

        // Allocate the first sequence of the second, the others can be calculated with the offset
        $firstSeqUid = $this->bitsAllocator->allocate($currentTime, 0);

        $uidList = [];
        for ($offset = 0; $offset < $listSize; $offset++) {
            $uidList[] = $firstSeqUid + $offset;
        }

        return $uidList;
    }
}