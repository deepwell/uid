<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Buffer\RingBuffer;

use Swoole\Table;

/**
 * each element of the array is a slot, which is being set with a UID
 */
class Slots
{

    protected Table $table;
    public function __construct(int $size)
    {
        $this->table = new Table($size * 2);
        $this->table->column('uid', Table::TYPE_INT);
        $this->table->create();
    }

    public function set(int $index, int $uid): bool
    {
        return $this->table->set((string)$index, ['uid' => $uid]);
    }

    public function get(int $index): int
    {
        return $this->table->get((string)$index, 'uid');
    }
}