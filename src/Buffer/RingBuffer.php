<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Buffer;

use Deepwell\HyperfUid\Buffer\RingBuffer\Flags;
use Deepwell\HyperfUid\Buffer\RingBuffer\Slots;
use Deepwell\HyperfUid\Event\ReachPaddingThreshold;
use Deepwell\HyperfUid\Utility\PaddedAtomicLong;

class RingBuffer extends AbstractBuffer
{
    /** Constants */
    final const int START_POINT = -1;
    final const int CAN_PUT_FLAG = 0;
    final const int CAN_TAKE_FLAG = 1;

    private readonly int $indexMask;

    /** Buffer's slots, each slot hold a UID */
    private Slots $slots;

    private readonly Flags $flags;

    /** Tail: last position sequence to produce */
    private readonly PaddedAtomicLong $tail;

    /** Cursor: current position sequence to consume */
    private readonly PaddedAtomicLong $cursor;

    public function __construct(int $bufferSize, int $paddingThreshold)
    {
        parent::__construct($bufferSize, $paddingThreshold);

        $this->indexMask = $this->bufferSize - 1;

        $this->tail = new PaddedAtomicLong(self::START_POINT);
        $this->cursor = new PaddedAtomicLong(self::START_POINT);

        $this->flags = $this->initFlags($this->bufferSize);

        $this->slots = new Slots($this->bufferSize);
    }

    /**
     * Initialize flags as CAN_PUT_FLAG
     */
    private function initFlags(int $bufferSize): Flags
    {
        $flags = new Flags();
        for ($i = 0; $i < $bufferSize; $i++) {
            $flags->push(new PaddedAtomicLong(self::CAN_PUT_FLAG));
        }
        return $flags;
    }

    /**
     * Take an UID of the ring at the next cursor, this is a lock free operation by using atomic cursor<p>
     *
     * Before getting the UID, we also check whether reach the padding threshold,
     * the padding buffer operation will be triggered in another thread<br>
     * @return int UID
     */
    public function take(): int
    {
        // spin get next available cursor
        $currentCursor = $this->cursor->get();
        $nextCursor = $this->cursorUpdateAndGet();

        // check for safety consideration, it never occurs
        assert($nextCursor >= $currentCursor, "Cursor can't move back");

        // trigger padding in an async-mode if reach the threshold
        if ($this->tail->get() - $nextCursor < $this->paddingThreshold) {
            $this->eventDispatcher->dispatch(new ReachPaddingThreshold($this));
        }

        // 1. check next slot flag is CAN_TAKE_FLAG
        $nextCursorIndex = $this->calSlotIndex($nextCursor);
        assert($this->flags[$nextCursorIndex]->get() == self::CAN_TAKE_FLAG, "Cursor not in can take status");

        // 2. get UID from next slot
        // 3. set next slot flag as CAN_PUT_FLAG.
        $uid = $this->slots->get($nextCursorIndex);
        $this->flags[$nextCursorIndex]->set(self::CAN_PUT_FLAG);

        // Note that: Step 2,3 can not swap. If we set flag before get value of slot, the producer may overwrite the
        // slot with a new UID, and this may cause the consumer take the UID twice after walk a round the ring
        return $uid;
    }

    protected function cursorUpdateAndGet(): int
    {
        $prev = $this->cursor->get();

        while (true) {

            $next = ($prev == $this->tail->get() ? $prev : $prev + 1);
            if ($this->cursor->cmpset($prev, $next)) {
                return $next;
            }

            $prev = $this->cursor->get();
        }
    }

    /**
     * Calculate slot index with the slot sequence
     */
    protected function calSlotIndex(int $sequence): int
    {
        return $sequence & $this->indexMask;
    }

    /**
     * Put an UID in the ring & tail moved<br>
     * We use 'synchronized' to guarantee the UID fill in slot & publish new tail sequence as atomic operations<br>
     *
     * <b>Note that: </b> It is recommended to put UID in a serialize way, cause we once batch generate a series UIDs and put
     * the one by one into the buffer, so it is unnecessary put in multi-threads
     */
    public function put(int $uid): bool
    {
        $currentCursor = $this->cursor->get();
        $currentTail = $this->tail->get();

        // tail catches the cursor, means that you can't put any cause of RingBuffer is full
        $distance = $currentTail - ($currentCursor == self::START_POINT ? 0 : $currentCursor);
        if ($distance == $this->bufferSize - 1) {
            return false;
        }

        // 1. pre-check whether the flag is CAN_PUT_FLAG.
        $nextTailIndex = $this->calSlotIndex($currentTail + 1);
        if ($this->flags[$nextTailIndex]->get() != self::CAN_PUT_FLAG) {
            return false;
        }

        // 2. put UID in the next slot
        // 3. update next slot's flag to CAN_TAKE_FLAG
        // 4. publish tail with sequence increase by one
        $this->slots->set($nextTailIndex, $uid);
        $this->flags[$nextTailIndex]->set(self::CAN_TAKE_FLAG);
        $this->tail->add();

        // The atomicity of operations above, guarantees by 'synchronized'. In another word,
        // the take operation can't consume the UID we just put, until the tail is published
        return true;
    }

    public function __toString()
    {
        return 'UidBuffer ' . json_encode([
                'cursor' => $this->cursor->get(),
                'tail' => $this->tail->get(),
                'paddingThreshold' => $this->paddingThreshold,
            ]);
    }
}