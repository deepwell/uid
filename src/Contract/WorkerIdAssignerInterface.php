<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Contract;

use Deepwell\HyperfUid\Worker\Input\BuildWorkerNodeInput;
use Deepwell\HyperfUid\Worker\Output\BuildWorkerNodeOutput;

interface WorkerIdAssignerInterface
{
    /**
     * Assign worker id for Generator
     */
    public function assignWorkerId(): int;

    public function buildWorkerNode(?BuildWorkerNodeInput $input = null): BuildWorkerNodeOutput;
}