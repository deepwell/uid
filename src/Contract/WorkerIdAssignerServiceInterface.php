<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Contract;

interface WorkerIdAssignerServiceInterface
{
    public function assignWorkerId(array $params): int;
}