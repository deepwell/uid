<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Contract;

/**
 * Represents a unique id generator.
 */
interface GeneratorInterface
{
    /**
     * @return void
     */
    public function init(): void;

    /**
     * Get a unique ID
     * @return int
     */
    public function getUid(): int;

    /**
     * Parse the UID into elements which are used to generate the UID. <br>
     * Such as timestamp & workerId & sequence...
     * @param int $uid
     */
    public function parseUid(int $uid);
}