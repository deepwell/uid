<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Contract;

/**
 * Buffered UID provider, which provides UID in the same one second
 */
interface BufferedUidProviderInterface
{
    public function provide(int $momentInSecond): array;
}