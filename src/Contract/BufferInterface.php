<?php
declare(strict_types=1);

namespace Deepwell\HyperfUid\Contract;

interface BufferInterface
{
    public function take(): int;

    public function put(int $uid): bool;
}