# 唯一ID生成器

## 介绍
参考[百度UidGenerator](https://github.com/baidu/uid-generator)方案, 基于PHP Hyperf框架的唯一ID生成器组件

## 安装
```
composer require deepwell/hyperf-uid
```

创建worker node数据表src/Worker/Mode/worker_node.sql

发布默认配置文件/config/autoload/deepwell_uid.php
```
php bin/hyperf.php vendor:publish deepwell/hyperf-uid
```

```
<?php

declare(strict_types=1);

return [
    // 当前时间,相对于时间基点的增量,单位秒
    'timestampBits' => 28,
    // 机器ID
    'workerIdBits' => 22,
    // 每秒下的并发序列
    'sequenceBits' => 13,
    // 时间基点 2022-11-26
    'epochSeconds' => 1669392000,
    // Buffer size扩容参数, 可提高UID生成的吞吐量
    // 原bufferSize=8192, 扩容后bufferSize= 8192 << 3 = 65536
    'boostPower' => 3,
    // 指定何时向Buffer中填充UID, 取值为百分比(0, 100), 默认为50
    // 举例: bufferSize=1024, paddingPercent=50 -> threshold=1024 * 50 / 100 = 512
    // 当通道中UID数量 < 512时, 将自动对Buffer进行填充补全
    'paddingPercent' => 50,
    // 另外一种Buffer填充时机, 周期性检查填充 -->
    // 默认:不配置此项, 即不使用用Schedule. 如需使用, 请指定Schedule时间间隔, 单位:秒 -->
    //'scheduleInterval'=>60,
];
```

## 使用方式

```
$generator = $this->container->get(GeneratorInterface::class);
$uid = $generator->getUid()
```


## 接口

### GeneratorInterface
生成器接口, 编排组装各个子模块, 获取UID, 解析UID

    DefaultGenerator - 常规雪花ID生成器

    CachedGenerator - 默认, 缓存式ID生成器


### WorkerIdAssignerInterface
woker id 注册器接口

    DisposableWorkerIdAssigner - 默认, 用完即弃的WorkerIdAssigner, 依赖DB操作


### BufferInterface
UID Buffer接口, CachedGenerator依赖此接口, 组件默认提供了一个环形数组RingBuffer


### BufferedUidProviderInterface
当buffer剩余UID数量达到阈值时, 会开启一个子协程填充buffer, 填充器需要此接口提供未来N秒的UID, 以填满buffer

